CREATE TABLE alumnos (
id int(3) NOT NULL,
nombre varchar(20)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO alumnos (id, nombre) VALUES(1, 'Paco');
INSERT INTO alumnos (id, nombre) VALUES(2, 'Juan');
ALTER TABLE alumnos ADD PRIMARY KEY (id);
