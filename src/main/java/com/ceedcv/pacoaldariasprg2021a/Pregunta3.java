/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceedcv.pacoaldariasprg2021a;

import java.util.Scanner;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 29 abr. 2021 13:05:32
 *
 */
public class Pregunta3 {

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args) {
      // TODO code application logic here

      String id;
      String nombre;

      Scanner in = new Scanner(System.in);

      System.out.print("Id: ");
      id = in.nextLine();

      System.out.print("Nombre: ");
      nombre = in.nextLine();

      try {
         Verifica(id);

      } catch (Miexcepcion ex) {
         //ex.printStackTrace();
         System.out.println(ex);
      }

   }

   private static void Verifica(String id_) throws Miexcepcion {
      int id;
      try {
         id = Integer.parseInt(id_);
      } catch (Exception e) {
         throw new Miexcepcion("Id incorrecto");
      }

   }

}
