/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceedcv.pacoaldariasprg2021a;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 29 abr. 2021 12:50:43
 */
public class Fichero {

   private String falumno = "alumnos.txt";

   public ArrayList leer() {

      ArrayList alumnos = new ArrayList();

      try {
         File f = new File(falumno);
         Scanner sc = new Scanner(f);
         int id;
         String nombre;

         while (sc.hasNext()) {
            String linea = sc.nextLine();
            String[] campos = linea.split(";");
            id = Integer.parseInt(campos[0]);
            nombre = campos[1];
            Alumno alumno = new Alumno(id, nombre);
            alumnos.add(alumno);
            
         }
         sc.close();

      } catch (Exception ex) {
         ex.printStackTrace();
      }

      return alumnos;

   }

   public void grabar(Alumno a) {
      String s;
      try {
         File f = new File(falumno);
         FileWriter fw = new FileWriter(f, true); // añadimos
         s = a.getId() + ";" + a.getNombre() + "\n";
         fw.write(s);
         System.out.println(s);
         fw.close();

      } catch (Exception ex) {
         ex.printStackTrace();
      }

   }

}
