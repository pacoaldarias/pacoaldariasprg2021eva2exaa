/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceedcv.pacoaldariasprg2021a;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 29 abr. 2021 12:35:19
 *
 */
public class Pregunta2 {

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args) {
      // TODO code application logic here
      ArrayList alumnos;

      Fichero fichero = new Fichero();
      alumnos = fichero.leer();
      mostrar(alumnos);

   }

   static void mostrar(ArrayList alumnos) {

      Iterator it = alumnos.iterator();
      Alumno a;

      while (it.hasNext()) {
         a = (Alumno) it.next();
         System.out.println(a);
      }

   }

}
