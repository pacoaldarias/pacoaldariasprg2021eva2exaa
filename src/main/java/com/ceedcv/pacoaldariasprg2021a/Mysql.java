/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceedcv.pacoaldariasprg2021a;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Mysql {

   private static Connection conn = null;

   public Mysql() {
      conectar();
   }

   /**
    * @param args the command line arguments
    */
   public static void conectar() {

      System.out.println("Conectando a la base de datos...");
      try {
         Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
         String url = "jdbc:mysql://172.20.0.2:3306/myDb?serverTimezone=UTC";
         conn = DriverManager.getConnection(url, "user", "test");
         System.out.println("OK!");
      } catch (SQLException e) {
         e.printStackTrace();
      } catch (ClassNotFoundException ex) {
         ex.printStackTrace();
      } catch (InstantiationException ex) {
         Logger.getLogger(Mysql.class.getName()).log(Level.SEVERE, null, ex);
      } catch (IllegalAccessException ex) {
         Logger.getLogger(Mysql.class.getName()).log(Level.SEVERE, null, ex);
      }

   }

   public static void grabar(Alumno a) {
      try {
         // Creamos un Statement scrollable y modificable
         Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                 ResultSet.CONCUR_UPDATABLE);
// Ejecutamos un SELECT y obtenemos la tabla clientes en un ResultSet
         String sql = "SELECT * FROM alumnos";
         ResultSet rs = stmt.executeQuery(sql);

         // Creamos un nuevo registro y lo insertamos
         rs.moveToInsertRow();
         rs.updateInt(1, a.getId());
         rs.updateString(2, a.getNombre());
         rs.insertRow();

         System.out.println("Insertado registro");
         // Cerramos el statement y la conexión
         stmt.close();
         conn.close();

      } catch (SQLException ex) {
         ex.printStackTrace();
      }
   }

}
